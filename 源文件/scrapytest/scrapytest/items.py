# Define here the models for your scraped items
#
# See documentation in:
# https://docs.scrapy.org/en/latest/topics/items.html

import scrapy


class ScrapytestItem(scrapy.Item):
    # define the fields for your item here like:
    # name = scrapy.Field()
    pass

class DoubanItem(scrapy.Item):
    Mid = scrapy.Field()
    name = scrapy.Field()
    movieMakers = scrapy.Field()
    Myear = scrapy.Field()
    Mcountry = scrapy.Field()
    Mtype = scrapy.Field()
    star = scrapy.Field()
    score = scrapy.Field()
    comments = scrapy.Field()
    desc = scrapy.Field()