from scrapy import cmdline

if __name__ == '__main__':
    # ## 提交启动爬虫命令行
    # cmdline.execute('scrapy crawl douban'.split())

    # ## 提交启动爬虫命令行：爬取信息导出到json 文件，参数：-O 覆盖任何现有文件，-o 将新内容附加到任何现有文件
    # cmdline.execute('scrapy crawl douban -O doubantop250.json'.split())

    ## 提交启动爬虫命令行：新内容附加到json 文件会是文件内容变为无效json。附加到文件时可以考虑json lines
    cmdline.execute('scrapy crawl douban -O doubantop250.jl'.split())